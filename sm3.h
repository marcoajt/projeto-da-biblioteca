struct nodo{
  int chave;
  struct nodo* prox;
  struct aresta* adj;
};typedef struct nodo Nodo;

struct grafo{
  int tam;
  struct nodo* lista_nodo;
};typedef struct grafo Grafo;

struct aresta{
  int chave_adj;
  int peso;
  int vem;
  struct aresta* prox;
};typedef struct aresta Aresta;

Grafo* insere_aresta(Grafo* g,int v,int a,int pe);
Grafo* cria_grafo(char *nome);
Nodo* insere_nodo(Nodo* a,int t);
void imprime(Grafo* g,int* t);
Aresta* criar(Aresta* a,int cha,int pes,int v);
Nodo* busca(Grafo *g,int item);
