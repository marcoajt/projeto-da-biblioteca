#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sm3.h"

Grafo* cria_grafo(char *nome){
  int ver,ares,peso,ta;
  char c;
  int i;


  char texto[500];
  FILE* arquivo;

  Grafo* g = (Grafo*)malloc(sizeof(Grafo));

  if((arquivo = fopen(nome,"r")) == NULL){
    printf("Permissao negada\n");
  }
  else{
    fscanf(arquivo,"%d\n",&ta);

    printf("%d\n",ta);


    //Grafo* g = (Grafo*)malloc(sizeof(Grafo));
    g->tam = ta;

    int l =0;

    Nodo* b = NULL;
    Aresta* z = NULL;

    //printf("OK\n");

    for(i = 0; i < ta; i++){
      b = insere_nodo(b,i + 1);
    }
    g->lista_nodo = b;

    while((c = fgetc(arquivo)) != EOF){
      if(c != '\n'){
        texto[l] = c;
        l++;
      }
      else{
        texto[l] = '\0';
        if(l > 0){
          ver = atoi(strtok(texto,";"));
          //printf("%d\n",ver);
          ares = atoi(strtok(NULL,";"));
          //printf("%d\n",ares);
          peso = atoi(strtok(NULL,";"));
          //printf("%d\n",peso);


          g = insere_aresta(g,ver,ares,peso);

        }
        l = 0;
        texto[l] = '\0';
      }
    }
	}
	fclose(arquivo);
	return g;
}


Nodo* insere_nodo(Nodo* a,int t){
  Nodo* novo = (Nodo*)malloc(sizeof(Nodo));
  novo->chave = t;
  novo->prox = a;
  return novo;
}

Aresta* criar(Aresta* a,int cha,int pes,int v){
	Aresta* novo = (Aresta*)malloc(sizeof(Aresta));
	novo->chave_adj = cha;
	novo->peso = pes;
	novo->prox = a;
  novo->vem = v;
	return novo;
}

Grafo* insere_aresta(Grafo* g,int v,int a,int pe){
  Nodo* p;
  Aresta* j;
  for(p = g->lista_nodo; p != NULL; p = p->prox){
    if(p->chave == v){
      j = p->adj;
      j = criar(j,a,pe,v);
      p->adj = j;
    }
  }
  return g;
}

void imprime(Grafo* g,int* t){
	Nodo* p;
	Aresta* j;

	for(p = g->lista_nodo; p != NULL; p = p->prox){
		printf("[%d]",p->chave);
		for(j = p->adj; j != NULL; j = j->prox){
			printf("-->[%d , peso: %d]",j->chave_adj,j->peso);
      (*t) = (*t)+1;
		}
		printf("\n");
	}
}



Nodo* busca(Grafo *g,int item){
	Nodo* aux = g->lista_nodo;
	while(aux != NULL){
		if(aux->chave == item){
			printf("encontrou o nodo %d\n",aux->chave);
			return aux;
		}
		aux = aux->prox;
	}
	return NULL;
}
