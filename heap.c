#include<stdio.h>
#include<stdlib.h>
#include"sm3.h"
#include"heap.h"


Heap **deleta_heap(int* t,Heap **vetor){
  int i,tam;
  tam = (*t)-1;
  Heap **vetoraux = (Heap**)malloc(sizeof(Heap*)*tam);
  for(i = 0;i < tam; i ++){
    vetoraux[i] = (Heap*)malloc(sizeof(Heap));
  }
  //vetor[0] = 0;
  for(i = 0; i < tam; i++){
    vetoraux[i]->peso = vetor[i + 1]->peso;
    vetoraux[i]->partida = vetor[i + 1]->partida;
    vetoraux[i]->chegada = vetor[i + 1]->chegada;
  }
  (*t) = (*t)-1;
  //sift_down(vetoraux,*t);
  return vetoraux;
}

void percolate(Heap **heap,int t){
  int i;
  Heap *aux = NULL;
  i = t-1;
  while(i != 0){
    if((heap[i]->peso) < (heap[i-1]->peso)){
      aux = heap[i-1];
      heap[i-1] = heap[i];
      heap[i] = aux;
    }
    i--;
  }
}

/*void sift_down(Heap **heap,int t){
  int i;
  Heap *aux = NULL;
  //printf("%d\n",t);
  for(i = 0; i < t; i++){
    if((heap[i]->peso) < (heap[i+1]->peso)){
      aux = heap[i];
      heap[i] = heap[i+1];
      heap[i+1] = aux;
    }
  }
}*/

Heap **criar_heap(int *t,Grafo *g,Heap **vetor,Aresta *j){
  int i,tam;
  tam = (*t) + 1;
  Heap **vetoraux = (Heap**)malloc(sizeof(Heap*)*tam);
  for(i = 0;i < tam;i ++){
    vetoraux[i] = (Heap*)malloc(sizeof(Heap));
  }
  for(i = 0;i < (*t);i ++){
    vetoraux[i] = vetor[i];
  }
  vetoraux = inserir_heap(vetoraux,tam,j->vem,j->chave_adj,j->peso);
  (*t) = (*t) + 1;
  return vetoraux;
}

Heap **inserir_heap(Heap **vetor,int tam,int partida,int chegada,int peso){
  vetor[tam - 1]->partida = partida;
  vetor[tam - 1]->chegada = chegada;
  vetor[tam - 1]->peso = peso;
  return vetor;
}

void imprimir_heap(Heap **vetor,int t){
  int i;
  for(i = 0;i < t;i ++){
    printf(" {%d}\t",vetor[i]->peso);
  }
  printf("\n");
  for(i = 0;i < t;i ++){
    printf("%d-->%d\t",vetor[i]->partida,vetor[i]->chegada);
  }
  printf("\n");
}
