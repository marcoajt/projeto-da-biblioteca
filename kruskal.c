#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sm3.h"
#include"heap.h"
#include"cd.h"
#include"kruskal.h"

void kruskal(Grafo *g,int tam,int *ts){
  int v,u,t = 0;
  int tam_heap = tam;
  int *conjunto;
  Heap **vetor;
  Heap **vfinal;
  Heap **S;
  Nodo *i;
  Aresta *j;
  for(i = g->lista_nodo;i != NULL;i = i->prox){
    for(j = i->adj;j != NULL;j = j->prox){
      vetor = criar_heap(&t,g,vetor,j);
      percolate(vetor,t);
    }
  }
  tam_heap = t;
  conjunto = cria_cd(g->tam);
  printf("\n----------HEAP INICIAL----------\n");
  imprimir_heap(vetor,t);
  printf("\n----------KRUSKAL----------\n");
  while(tam_heap > 0){
  //  v = vetor[0]->partida; //aqui marco
   // u = vetor[0]->chegada; // aqui marco
    v=findSet(vetor[0]->partida, conjunto);
    u=findSet(vetor[0]->chegada, conjunto);
    if(v != u){
      (*ts) = (*ts) + 1;
      S = insere_s(vetor,S,*ts);
      printf("\n----------SOLUCAO----------\n");
      imprime_s(S,*ts);
      unionn(v,u,conjunto,g->tam);
    }
    vetor = deleta_heap(&t,vetor);
    printf("\n----------HEAP----------\n");
    imprimir_heap(vetor,t);
    tam_heap = t;
  }
  printf("\n----------SOLUCAO FINAL----------\n");
  imprime_s(S,*ts);
}

Heap **insere_s(Heap **vetor,Heap **S,int t){
  int i;
  Heap **aux = (Heap**)malloc(sizeof(Heap*)*t);
  for(i = 0;i < (t - 1); i ++){
    aux[i] = S[i];
  }
  aux[t-1] = vetor[0];
  return aux;
}

void imprime_s(Heap **vetor,int t){
  int i;
  for(i = 0;i < t;i ++){
    printf(" {%d}\t",vetor[i]->peso);
  }
  printf("\n");
  for(i = 0;i < t;i ++){
    printf("%d-->%d\t",vetor[i]->partida,vetor[i]->chegada);
  }
  printf("\n");
}
