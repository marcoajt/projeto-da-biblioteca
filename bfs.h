struct lista{
  int info;
  struct lista* prox;
};typedef struct lista Lista;

struct fila{
  Lista* ini;
  Lista* fim;
  int tamanho;
};typedef struct fila Fila;

Fila* fila_cria();
void fila_insere(Fila* f,int v);
int fila_retira(Fila* f);
void bfs(Grafo* g,int v);
Nodo* busca(Grafo *g,int item);
