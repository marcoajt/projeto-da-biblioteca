index:
	gcc sm3.c -c
	gcc kruskal.c -c
	gcc heap.c -c
	gcc cd.c -c
	gcc bfs.c -c
	gcc dfs.c -c
	gcc prim.c -c
	gcc dijkstra.c -c
	gcc main.c sm3.o kruskal.o heap.o cd.o bfs.o dfs.o prim.o dijkstra.o -o exec
	./exec
gdb:
	gcc sm3.c -g -c
	gcc kruskal.c -g -c
	gcc heap.c -g -c
	gcc cd.c -g -c
	gcc main.c -g sm3.o kruskal.o heap.o cd.o -o exec
	gdb exec
