#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sm3.h"
#include"heap.h"
#include"dijkstra.h"

void dijkstra(Grafo *g,int ini){
  int t = 0;
    int *pai = malloc(sizeof(int)*(g->tam));
    int *solucao = malloc(sizeof(int)*(g->tam));
    int *distancia = malloc(sizeof(int)*(g->tam));

    for(int i = 0; i < g->tam; i++){
        distancia[i] = 10000;
        pai[i] = 0;
        solucao[i] = 0;
    }
    distancia[ini-1] = 0;
    Nodo *nodo = busca(g,ini);
    Aresta *a = nodo->adj;
    Heap **heap;
    while(a != NULL){
        heap = criar_heap(&t,g,heap,a);
        a = a->prox;
    }
    printf("\n----------HEAP----------\n");
    imprimir_heap(heap,t);

    int cont = 0;
    while (t > 0) {
      Nodo *v = busca(g,heap[0]->chegada);
      Nodo *u = busca(g,heap[0]->partida);
      int p = heap[0]->peso;
      //printf("%d\n",p);
        heap = deleta_heap(&t,heap);
        if (!revisaS(solucao, u->chave, g->tam)){
            solucao[cont] = u->chave;
        }
        if ((distancia[v->chave-1] > (distancia[u->chave-1] + p) && (!revisaS(solucao, v->chave, g->tam))) || (distancia[v->chave-1]) > (distancia[u->chave-1] + p) ){
            distancia[v->chave-1] = distancia[u->chave-1] + p;
            pai[v->chave] = u->chave;
            for (Aresta *a = v->adj; a != NULL; a = a->prox){
                heap = criar_heap(&t,g,heap,a);
                printf("\n----------HEAP----------\n");
                imprimir_heap(heap,t);
            }
        }
        cont++;
    }
    printf("\n----------SOLUCAO----------\n");
    for (int i = 0; i < g->tam; i++) {
        printf("[%d]--%d--[%d]\n",ini, distancia[i], i + 1);
    }
    heap = deleta_heap(&t,heap);
}

int revisaS(int *s, int v, int tam) {
    int i = 0;
    while (i < tam) {
        if (s[i] == v) {
            return 1;
        }
        i++;
    }
    return 0;
}
