#include<stdio.h>
#include<stdlib.h>
#include"cd.h"

void make_set(int N,int* conjunto){
  int i;
  for(i = 1; i <= N; i++){
    conjunto[i] = i;
  }
}

int* cria_cd(int N){
  int* vetor = (int*)malloc(sizeof(int)*N);
  make_set(N,vetor);
  return vetor;
}

void imprime_cd(int N,int* v){
  int i;
  for(i = 1; i <= N; i++){
    printf("conj[%d] = %d\t",i,v[i]);
  }
  printf("\n");
}

void unionn(int i,int j,int* conjunto,int t){
  int x;
  if(conjunto[j] == j){
    conjunto[j] = findSet(i,conjunto);
    for(x = 1;x <= t;x ++){
      if(conjunto[x] == j){
        conjunto[x] = conjunto[j];
      }
    }
  }
  else{
    conjunto[i] = findSet(j,conjunto);
    for(x = 1;x <= t;x ++){
      if(conjunto[x] == i){
        conjunto[x] = conjunto[i];
      }
    }
  }
}

int findSet(int i,int* conjunto){
  if(conjunto[i] == i){
    return i;
  }
  else{
    conjunto[i] = findSet(conjunto[i],conjunto);
    return conjunto[i];
  }
}
