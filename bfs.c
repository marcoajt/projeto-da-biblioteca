#include<stdio.h>
#include<stdlib.h>
#include"sm3.h"
#include"bfs.h"
#include"dfs.h"

void bfs(Grafo* g,int v){
  int i,s = 1,r = 0;
  int vetorS[g->tam+1];
  int *solucao = (int*)malloc(sizeof(int)*(g->tam));
  Fila* f = fila_cria();
  for(i = 0; i <= (g->tam); i++){
    vetorS[i] = 0;
  }
  fila_insere(f,v);

	while(f->tamanho > 0){
		r = fila_retira(f);
		if(vetorS[r] == 0){
			vetorS[r] = s;
      solucao[s] = r;
			s++;
			Nodo* temp2 = busca(g,r);
			if(temp2 != NULL){
				Aresta* aux2 = temp2->adj;
				while(aux2!= NULL){
					printf("inserindo adjacencia %d\n",aux2->chave_adj);
					fila_insere(f,aux2->chave_adj);
					aux2 = aux2->prox;
				}
			}
			else{
					printf("busca é null\n");
			}
		}
	}
    for(i = 1; i <= (g->tam); i++){
      printf("%d\t",solucao[i]);
    }
    printf("\n");
}

Fila* fila_cria(){
  Fila* f = (Fila*)malloc(sizeof(Fila));
  f->ini = f->fim = NULL;
  return f;
}

void fila_insere(Fila* f,int v){
  Lista* novo = (Lista*)malloc(sizeof(Lista));
  novo->info = v;
  novo->prox = NULL;
  if(f->fim == NULL && f->ini == NULL){
    f->ini = novo;
    f->fim = novo;
  }
  else{
    f->fim->prox = novo;
    f->fim = novo;
  }
  f->tamanho++;
}

int fila_retira(Fila* f){
  Lista* t;
  int v;
  if(f->fim == NULL && f->ini == NULL){
    printf("Fila vazia!\n");
    exit(1);
  }

  t = f->ini;
  v = t->info;
  f->ini = t->prox;
  if(f->ini == NULL){
    f->fim = NULL;
  }
  free(t);
  f->tamanho--;
  return v;
}
