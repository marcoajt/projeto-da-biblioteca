#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sm3.h"
#include"heap.h"
#include"kruskal.h"
#include"bfs.h"
#include"dfs.h"
#include"prim.h"
#include"dijkstra.h"

int main(){
  int op,ini,v;
  int th = 0;
  int ts = 0;
  char nome[20];
  Grafo* a;
  while(op != 7){
    printf("\n----------MENU----------\n");
    printf("1)Criar grafo\n2)Kruskal\n3)Prim\n4)Dijkstra\n5)DFS\n6)BFS\n7)SAIR\n");
    scanf("%d",&op);
    switch(op){
      case 1:{
        printf("Digite o nome do arquivo: ");
        scanf("%s[^\n]",nome);
        a = cria_grafo(nome);
        imprime(a,&th);
        break;
      }
      case 2:{
        kruskal(a,th,&ts);
        break;
      }
      case 3:{
        printf("Onde deseja iniciar: ");
        scanf("%d",&ini);
        prim(a,ini);
        break;
      }
      case 4:{
        printf("Onde deseja iniciar: ");
        scanf("%d",&ini);
        dijkstra(a,ini);
        break;
      }
      case 5:{
        printf("Digite onde iniciar: ");
        scanf("%d",&v);
        dfs(a,v);
        break;
      }
      case 6:{
        printf("Digite onde iniciar: ");
        scanf("%d",&v);
        bfs(a,v);
        break;
      }
    }
  }
  return 0;
}
