#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sm3.h"
#include"cd.h"
#include"heap.h"
#include"prim.h"

void prim(Grafo *g,int ini){
  int t = 0;
  int ts = 0;
  int v,u,d;
  int *conjunto = cria_cd(g->tam);
  Heap **heap;
  Heap **solucao;
  Nodo *i = busca(g,ini);
  Aresta *j;
  for(j = i->adj;j != NULL;j = j->prox){
    //t = t + 1;
    heap = criar_heap(&t,g,heap,j);
    percolate(heap,t);
    printf("\n----------HEAP----------\n");
    imprimir_heap(heap,t);
    printf("\n-------------------------\n");
  }
  while(t > 0){
    //v = findSet(heap[0]->partida, conjunto);
    //u = findSet(heap[0]->chegada, conjunto);
    v = heap[0]->partida;
    u = heap[0]->chegada;
    d = heap[0]->peso;
    heap = deleta_heap(&t,heap);
    printf("\n----------HEAP DELETA----------\n");
    imprimir_heap(heap,t);
    printf("\n-------------------------\n");
    if(conjunto[v] != conjunto[u]){
      Nodo *i = busca(g,u);
      Aresta *j;
      for(j = i->adj;j != NULL;j = j->prox){
        //t = t + 1;
        heap = criar_heap(&t,g,heap,j);
        percolate(heap,t);
        printf("\n----------HEAP----------\n");
        imprimir_heap(heap,t);
        printf("\n-------------------------\n");
      }
      unionn(v,u,conjunto,g->tam);
      solucao = inserir_solucao(&ts,solucao,v,u,d);
      printf("\n----------SOLUCAO----------\n");
      imprimir_heap(solucao,ts);
      printf("\n---------------------------\n");
    }
  }
  printf("\n----------SOLUCAO FINAL----------\n");
  imprimir_heap(solucao,ts);
  printf("\n----------------------------------\n");
}

Heap **inserir_solucao(int *t,Heap **solucao,int v,int u,int d){
  int i;
  (*t)++;
  Heap **novo = (Heap**)malloc(sizeof(Heap*));
  for(i = 0;i < (*t);i ++){
    novo[i] = (Heap*)malloc(sizeof(Heap));
  }
  for(i = 0;i < (*t)-1;i ++){
    novo[i] = solucao[i];
  }
  novo = inserir_heap(novo,(*t),v,u,d);
  return novo;
}
