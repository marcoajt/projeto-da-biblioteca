struct heap{
  int partida;
  int chegada;
  int peso;
};typedef struct heap Heap;

void percolate(Heap **heap,int t);
Heap **criar_heap(int *t,Grafo *g,Heap **vetor,Aresta *j);
void imprimir_heap(Heap **vetor,int t);
Heap **inserir_heap(Heap **vetor,int tam,int partida,int chegada,int peso);
Heap **deleta_heap(int* t,Heap **vetor);
//void sift_down(int **heap,int t);
