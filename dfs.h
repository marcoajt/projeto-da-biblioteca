struct lista2{
  int info;
  struct lista2 *prox;
};typedef struct lista2 Listap;

struct pilha{
  Listap *topo;
  int tamanho;
};typedef struct pilha Pilha;

void dfs(Grafo* g,int v);
Pilha* pilha_cria();
void pilha_push(Pilha* p,int v);
int pilha_pop(Pilha* p);
void imprimePilha(Pilha *p);
void imprimePilhaTamanho(Pilha *p);
