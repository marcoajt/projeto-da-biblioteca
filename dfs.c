#include <stdio.h>
#include <stdlib.h>
#include "sm3.h"
#include "bfs.h"
#include "dfs.h"

void dfs(Grafo* g,int v){
  int i,s = 1,r = 0;
  int vetorS[g->tam+1];
  int* solucao = (int*)malloc(sizeof(int)*(g->tam));
  Pilha* p = pilha_cria();
  for(i = 0; i <= (g->tam); i++){
    vetorS[i] = 0;
  }
    pilha_push(p,v);
	while(p->tamanho > 0){
		r = pilha_pop(p);
		if(vetorS[r] == 0){
			vetorS[r] = s;
      solucao[s] = r;
			s++;
			Nodo* temp2 = busca(g,r);
			if(temp2 != NULL){
				Aresta* aux2 = temp2->adj;
				while(aux2!= NULL){ //PRINCIPAL PROBLEMA AQUI NUNCA VARRIA AS ADJACENCIAS E SEMPRE CONSIDERAVA MESMO NODO.
					printf("inserindo adjacencia %d\n",aux2->chave_adj);
					pilha_push(p,aux2->chave_adj);
					aux2 = aux2->prox;
				}
			}
			else{
					printf("busca é null\n");
					}
		}
	}
    for(i = 1; i <= (g->tam); i++){
      printf("%d\t",solucao[i]);
    }
    printf("\n");
}

Pilha* pilha_cria(){
  Pilha* p = (Pilha*)malloc(sizeof(Pilha));
  p->topo = NULL;
  p->tamanho =0;
  return p;
}

void pilha_push(Pilha* p,int v){
  Listap* novo = (Listap*)malloc(sizeof(Listap));
  novo->info = v;
  novo->prox = p->topo;
  p->topo = novo;
  p->tamanho++;
}

int pilha_pop(Pilha* p){
  Listap* aux;
  int v;
  if(p->topo != NULL){
    aux = p->topo;
    v = aux->info;
    p->topo = p->topo->prox;
    p->tamanho--;
    free(aux);
  }
  return v;
}


void imprimePilha(Pilha *p){
  printf("------PILHA-------\n");
  Listap *temp = p->topo;
  while(temp !=NULL){
    printf("%d - ",temp->info);
    temp = temp->prox;
  }
  printf("\n------------------\n");

}

void imprimePilhaTamanho(Pilha *p){
  printf("------PILHA TAMANHO-------\n");
  printf("%d\n",p->tamanho);
  printf("------------------\n");
}
